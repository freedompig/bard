﻿using UnityEngine;
using UnityEditor;

public class CreateSetting
{
    [MenuItem("Assets/Create/Setting")]
    public static void CreateAsset()
    {
        ScriptableObjectUtility.CreateAsset<Setting>();
    }
}