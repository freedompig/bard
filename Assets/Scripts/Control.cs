﻿using UnityEngine;
using System.Collections;

public enum ControlType
{
    FREE
}

public interface IControl
{
    void Init();
}

public class FreeControl : MonoBehaviour, IControl
{
    protected Player player;

    const float deadzone = 0.05f;
    protected float speed;
    protected float m_x = 0.0f;
    protected float m_y = 0.0f;
    protected Vector3 dir = Vector3.forward;

    public void Init()
    {
        player = GetComponent<Player>();
        speed = player.MoveSpeed;
    }

    public void Update()
    {
        if (GamepadManager.Instance.GetGamepad(player.PlayerID).IsConnected)
        {
            m_x = GamepadManager.Instance.GetGamepad(player.PlayerID).GetStick_L().X;
            m_y = GamepadManager.Instance.GetGamepad(player.PlayerID).GetStick_L().Y;
        }
        else
        {
            if(player.PlayerID == 0)
            {
                m_x = Input.GetAxis("Horizontal");
                m_y = Input.GetAxis("Vertical");
            }
        }

        if (m_x > deadzone || m_x < -deadzone)
        {
            this.transform.position = this.transform.position + new Vector3(m_x * Time.deltaTime * speed, 0.0f, 0.0f);
        }
        if (m_y > deadzone || m_y < -deadzone)
        {
            this.transform.position = this.transform.position + new Vector3(0.0f, 0.0f, m_y * Time.deltaTime * speed);
        }
        float angle = 0.0f;
        if (m_x < 0)
        {
            dir = -Vector3.forward;
            angle += 180.0f;
        }
        angle += Vector3.Angle(dir, new Vector3(m_x, 0.0f, m_y));

        this.transform.rotation = Quaternion.AngleAxis(angle, Vector3.up);
    }
}