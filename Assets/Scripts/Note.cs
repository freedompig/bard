﻿using UnityEngine;
using System.Collections;

public class Note : MonoBehaviour {

    public float ProjForce;
    public int TeamID { get; set; }

	// Use this for initialization
	void Start () {
        //Destroy(this.gameObject, 2.0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        Walker walker = other.GetComponent<Walker>();
        if (walker != null)
        {
            if(walker.TeamID == TeamID)
            {
                other.transform.Rotate(Vector3.up, 10.0f);
                Destroy(this.gameObject);
            }
        }
    }
}
