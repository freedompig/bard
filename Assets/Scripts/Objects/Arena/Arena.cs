﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class Arena : MonoBehaviour {
    public Transform Center;

    public Dictionary<int, Player> Players { get; private set; }
    public Dictionary<int, Team> Teams { get; private set; }

    public List<SpawnPosition> PlayerSpawnPositions;
    public List<SpawnPosition> WalkerSpawnPositions;

    void Awake()
    {
        Players = new Dictionary<int, Player>();
        Teams = new Dictionary<int, Team>();
    }

    public void Init()
    {
        foreach (PlayerConfig c in GameManager.Instance.PlayerConfigs)
        {
            Object playerPrefab = Resources.Load(string.Format("Prefabs/Players/{0}", c.PlayerClass));
            SpawnPosition spawnPos = PlayerSpawnPositions.Find(x => x.ID == c.PlayerID);
            Player player = ((GameObject)Instantiate(playerPrefab, spawnPos.Pos.position, Quaternion.identity)).GetComponent<Player>();
            player.PlayerID = c.PlayerID;
            player.TeamID = c.TeamID;
            player.SpawnPosition = spawnPos;
            Players.Add(c.PlayerID, player.GetComponent<Player>());

            if (!Teams.ContainsKey(c.TeamID))
            {
                Team team = new Team();
                team.TeamID = c.TeamID;
                Object walkerPrefab = Resources.Load(string.Format("Prefabs/Walkers/{0}", c.WalkerClass));
                Walker walker = ((GameObject)Instantiate(walkerPrefab, WalkerSpawnPositions.Find(x => x.ID == c.TeamID).Pos.position, Quaternion.identity)).GetComponent<Walker>();
                team.Walker = walker;
                walker.TeamID = c.TeamID;
                Teams.Add(team.TeamID, team);
            }

            player.Init();
        }
    }

    public void Deinitialise()
    {
        foreach(Player p in Players.Values)
        {
            Destroy(p.gameObject);
        }
        foreach(Team t in Teams.Values)
        {
            Destroy(t.Walker.gameObject);
        }
    }

    // Use this for initialization
    void Start ()
    {
        Debug.Log("Arena - Start");

    }

    // Update is called once per frame
    void Update () {
	
	}
}