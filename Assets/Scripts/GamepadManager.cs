﻿using UnityEngine;
using System.Collections.Generic;

public class GamepadManager : Singleton<GamepadManager> {

    public List<x360_Gamepad> gamepads { get; private set; }

    void Awake()
    {
        gamepads = new List<x360_Gamepad>();
        for (int i = 0; i < 4; ++i)
        {
            gamepads.Add(new x360_Gamepad(i));
        }
    }

    // Use this for initialization
    void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {
        foreach (x360_Gamepad x in gamepads)
        {
            x.Update();
        }
    }

    void LateUpdate()
    {
        Refresh();
    }

    public void Refresh()
    {
        foreach (x360_Gamepad x in gamepads)
        {
            x.Refresh();
        }
    }

    // Return specified gamepad
    // (Pass index of desired gamepad, eg. 1)
    public x360_Gamepad GetGamepad(int index)
    {
        for (int i = 0; i < gamepads.Count;)
        {
            // Indexes match, return this gamepad
            if (gamepads[i].Index == index)
                return gamepads[i];
            else
                ++i;
        }

        Debug.LogError("[GamepadManager]: " + index + " is not a valid gamepad index!");

        return null;
    }

    // Return number of connected gamepads
    public int ConnectedTotal()
    {
        int total = 0;

        for (int i = 0; i < gamepads.Count; ++i)
        {
            if (gamepads[i].IsConnected)
                total++;
        }

        return total;
    }

    // Check across all gamepads for button press.
    // Return true if the conditions are met by any gamepad
    public bool GetButtonAny(string button)
    {
        for (int i = 0; i < gamepads.Count; ++i)
        {
            // Gamepad meets both conditions
            if (gamepads[i].IsConnected && gamepads[i].GetButton(button))
                return true;
        }

        return false;
    }

    // Check across all gamepads for button press - CURRENT frame.
    // Return true if the conditions are met by any gamepad
    public bool GetButtonDownAny(string button)
    {
        for (int i = 0; i < gamepads.Count; ++i)
        {
            // Gamepad meets both conditions
            if (gamepads[i].IsConnected && gamepads[i].GetButtonDown(button))
                return true;
        }

        return false;
    }
}
