﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour
{
    public float MoveSpeed;
    public GameObject Forward;
    public GameObject Note;
    private IControl control;
    private Aiming aiming;
    private ControlType controlType;
    private AimingType aimingType;

    public int PlayerID { get; set; }
    public int TeamID { get; set; }
    public SpawnPosition SpawnPosition { get; set; }

    // Use this for initialization
    void Awake () {
    }

    public void Init()
    {
        controlType = GameManager.Instance.ControlType;
        aimingType = GameManager.Instance.AimingType;

        //To prevent Unity from creating multiple copies of the same component in inspector at runtime
        Component c = GetComponent<IControl>() as Component;
        if (c != null)
        {
            Destroy(c);
        }
        switch (controlType)
        {
            case ControlType.FREE:
                control = gameObject.AddComponent<FreeControl>();
                break;
            default:
                break;
        }
        control.Init();

        //To prevent Unity from creating multiple copies of the same component in inspector at runtime
        c = GetComponent<Aiming>() as Component;
        if (c != null)
        {
            Destroy(c);
        }
        switch (aimingType)
        {
            case AimingType.CENTER:
                aiming = gameObject.AddComponent<CenterAiming>();
                break;
            case AimingType.FIXED:
                aiming = gameObject.AddComponent<FixedAiming>();
                break;
            case AimingType.TARGETTED:
                aiming = gameObject.AddComponent<TargettedAiming>();
                break;
        }
        aiming.Init();
    }

    // Update is called once per frame2
    void Update () {
	
	}
}
