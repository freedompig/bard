﻿using UnityEngine;
using System.Collections;

public class GameTimer {

    protected float m_runningTime;
    protected bool m_running;

    public float Time { get { return m_runningTime; } }
    
    public void Start()
    {
        m_running = true;
    }

    public void Stop()
    {
        m_running = false;
    }

    public void Reset()
    {
        m_runningTime = 0.0f;
    }

    public void Update(float delta)
    {
        if(m_running)
        {
            m_runningTime += delta;
        }
    }
}
