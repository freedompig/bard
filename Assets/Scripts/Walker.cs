﻿using UnityEngine;
using System.Collections;

public class Walker : MonoBehaviour {
    public float velocity;
    public float walkTime;
    protected GameTimer walkTimer;
    public int TeamID { get; set; }

    // Use this for initialization
    void Start () {
        walkTimer = new GameTimer();
        walkTimer.Start();
	}
	
	// Update is called once per frame
	void Update () {
        //walkTimer.Update(Time.deltaTime);
        //if (walkTimer.Time > walkTime)
        //{
        //    GetComponent<Rigidbody>().AddForce(this.transform.forward * 200.0f);
        //    walkTimer.Reset();
        //}

        GetComponent<Rigidbody>().velocity = this.transform.forward * velocity;
	}
}
