﻿using UnityEngine;
using System.Collections;

public class PlayerConfig
{
    public int PlayerID;
    public int TeamID;
    public string PlayerClass;
    public string WalkerClass;
}
