﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : Singleton<GameManager>
{
    protected GameManager() { } // guarantee this will be always a singleton only - can't use the constructor!

    public int Players;
    public List<Setting> Settings;

    private int currentSetting = 0;
    public Arena Arena { get; private set; }
    public GameObject Player { get; private set; }
    public ControlType ControlType { get { return Settings[currentSetting].ControlType; } }
    public AimingType AimingType { get { return Settings[currentSetting].AimingType; } }

    public List<PlayerConfig> PlayerConfigs { get; private set; }

    void Awake ()
    {
        PlayerConfigs = new List<PlayerConfig>();
        for (int i = 0; i < Players; ++i)
        {
            PlayerConfig playerConfig = new PlayerConfig();
            playerConfig.PlayerID = i;
            playerConfig.TeamID = i;
            playerConfig.PlayerClass = "ClaireSphere";
            playerConfig.WalkerClass = "Walker";
            PlayerConfigs.Add(playerConfig);
        }
    }

    // Use this for initialization
    void Start () {
        Init();
    }

    public void Init()
    {
        Arena = ((GameObject)Instantiate(Settings[currentSetting].Arena)).GetComponent<Arena>();
        Arena.Init();
    }

    public void Deinitialise()
    {
        Arena.Deinitialise();
        Destroy(Arena.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Joystick1Button7) || Input.GetKeyUp(KeyCode.Return))
        {
            Deinitialise();
            currentSetting = (currentSetting + 1) % Settings.Count;
            Init();
        }
    }
}
