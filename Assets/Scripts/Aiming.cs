﻿using UnityEngine;
using System.Collections;

public enum AimingType
{
    CENTER,
    FIXED,
    TARGETTED
}

public abstract class Aiming : MonoBehaviour
{
    protected Player player;
    public abstract void Init();

    void Awake()
    {
        player = GetComponent<Player>();
    }
}

public class CenterAiming : Aiming
{
    private GameObject note;
    private Transform walker;
    private Transform center;

    // Use this for initialization
    void Start()
    {
    }

    public override void Init()
    {
        note = player.Note;
        walker = GameManager.Instance.Arena.Teams[player.TeamID].Walker.gameObject.transform;
        center = GameManager.Instance.Arena.GetComponent<Arena>().Center.transform;
    }

    // Update is called once per frame
    void Update()
    {
            if (((GamepadManager.Instance.GetGamepad(player.PlayerID).IsConnected) && (GamepadManager.Instance.GetGamepad(player.PlayerID).GetButtonDown("A") || GamepadManager.Instance.GetButtonDownAny("Start"))) ||
                (player.PlayerID == 0 && Input.GetKeyDown(KeyCode.Space)))
            {
                Vector3 vecWalker = walker.position - transform.position;
                Vector3 vecCenter = center.position - transform.position;

                float angle = Math.AngleSigned(vecCenter, vecWalker, Vector3.up);

                Quaternion rotation = Quaternion.AngleAxis(-angle, Vector3.up);
                Vector3 adjustedAim = rotation * vecWalker;

                GameObject proj = Instantiate(note, this.transform.position, this.transform.rotation) as GameObject;
                Note noteObj = proj.GetComponent<Note>();
                noteObj.TeamID = player.TeamID;
                proj.GetComponent<Rigidbody>().AddForce((adjustedAim).normalized * noteObj.ProjForce);
            }
    }
}

public class FixedAiming : Aiming
{
    private GameObject note;
    private Transform walker;

    protected Vector3 localRight = Vector3.zero;

    // Use this for initialization
    void Start()
    {
    }

    public override void Init()
    {
        note = player.Note;
        walker = GameManager.Instance.Arena.Teams[player.TeamID].Walker.gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        if (((GamepadManager.Instance.GetGamepad(player.PlayerID).IsConnected) && (GamepadManager.Instance.GetGamepad(player.PlayerID).GetButtonDown("A") || GamepadManager.Instance.GetButtonDownAny("Start"))) ||
            (player.PlayerID == 0 && Input.GetKeyDown(KeyCode.Space)))
        {
            Vector3 vecWalker = walker.position - transform.position;
            Vector3 fixedRight = Quaternion.AngleAxis(90, Vector3.up) * player.SpawnPosition.Dir;
            float angle = Math.AngleSigned(player.SpawnPosition.Dir, vecWalker, fixedRight);

            Quaternion rotation = Quaternion.AngleAxis(angle, fixedRight);
            Vector3 adjustedAim = rotation * player.SpawnPosition.Dir;

            GameObject proj = Instantiate(note, this.transform.position, this.transform.rotation) as GameObject;
            Note noteObj = proj.GetComponent<Note>();
            noteObj.TeamID = player.TeamID;
            proj.GetComponent<Rigidbody>().AddForce((adjustedAim).normalized * noteObj.ProjForce);
        }
    }
}

public class TargettedAiming : Aiming {
    private GameObject note;
    private Transform walker;

    // Use this for initialization
    void Start () {
	}

    public override void Init()
    {
        note = player.Note;
        walker = GameManager.Instance.Arena.Teams[player.TeamID].Walker.gameObject.transform;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (((GamepadManager.Instance.GetGamepad(player.PlayerID).IsConnected) && (GamepadManager.Instance.GetGamepad(player.PlayerID).GetButtonDown("A") || GamepadManager.Instance.GetButtonDownAny("Start"))) ||
            (player.PlayerID == 0 && Input.GetKeyDown(KeyCode.Space)))
        {
            GameObject proj = Instantiate(note, this.transform.position, this.transform.rotation) as GameObject;
            Note noteObj = proj.GetComponent<Note>();
            noteObj.TeamID = player.TeamID;
            proj.GetComponent<Rigidbody>().AddForce((walker.position - this.transform.position).normalized * noteObj.ProjForce);
        }
	}
}
