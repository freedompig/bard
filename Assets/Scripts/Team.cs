﻿using UnityEngine;
using System.Collections;

public class Team {

    public int TeamID { get; set; }
    public Walker Walker { get; set; }
}
