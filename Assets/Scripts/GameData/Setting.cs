﻿using UnityEngine;
using System.Collections;

public class Setting : ScriptableObject {
    public GameObject Arena;
    public GameObject Player;
    public Vector3 PlayerPos;
    public GameObject Walker;
    public Vector3 WalkerPos;
    public ControlType ControlType;
    public AimingType AimingType;
}