﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SpawnPosition
{
    public int ID;
    public Transform Pos;
    public Vector3 Dir;
}